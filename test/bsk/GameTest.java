package bsk;

import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import java.util.Arrays;
import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	
	@Test
	public void testInitializateGame() throws BowlingException  {
		Game game= new Game();
		String s="[ , ] [ , ] [ , ] [ , ] [ , ] [ , ] [ , ] [ , ] [ , ] [ , ]";
		assertEquals(s,game.getgame());
	}

	@Test
	public void testAddFrame() throws BowlingException {
		Game game= new Game();
		String s="[1,5] [3,6] [7,2] [3,6] [4,4] [5,3] [3,3] [4,5] [8,1] [2,6]";
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(s,game.getgame());
	}
	
	
	@Test
	public void testGetFrameAtIndex() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		String s="[7,2]";
		assertEquals(s,game.getFrameAt(2).getFrame());
	}
	
	@Test
	public void testGetFrameAtIndex2() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		Frame f=new Frame(7,2);
		assertTrue((f.getFirstThrow()==game.getFrameAt(2).getFirstThrow())&&(f.getSecondThrow()==game.getFrameAt(2).getSecondThrow()));
	}
	
	@Test
	public void testGetGameScore() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWhitSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWhitStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWWhitStrikeandSingleFrame() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		assertEquals(10,game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWhitStrikeAndSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testGetGameScoreWhitDoubleStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(112,game.calculateScore());
	}
		
	@Test
	public void testGetGameScoreWhitDoubleSpere() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testBonusLastFrameSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		Frame frame=new Frame(7,0);
		int i=game.getIndex();
		assertTrue((i==10) && (game.getFrameAt(i-1).isSpare()));
		game.setFirstBonusThrow(frame.getFirstThrow());
		assertEquals(17, 10+game.getFirstBonusThrow());		
	}
	
	@Test
	public void testGetGmaeScoreLastFrameSpare() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		Frame frame=new Frame(7,0);
		int i=game.getIndex();
		assertTrue((i==10) && (game.getFrameAt(i-1).isSpare()));
		game.setFirstBonusThrow(frame.getFirstThrow());
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testBonusLastFrameStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		Frame frame=new Frame(7,2);
		int i=game.getIndex();
		assertTrue((i==10) && (game.getFrameAt(i-1).isStrike()));
		game.setFirstBonusThrow(frame.getFirstThrow());
		game.setSecondBonusThrow(frame.getSecondThrow());
		assertEquals(19,10+game.getSecondBonusThrow());		
	}
	
	@Test
	public void testGetGmaeScoreLastFrameStrike() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		Frame frame=new Frame(7,2);
		int i=game.getIndex();
		assertTrue((i==10) && (game.getFrameAt(i-1).isStrike()));
		game.setFirstBonusThrow(frame.getFirstThrow());
		game.setSecondBonusThrow(frame.getSecondThrow());
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testGetGmaeScorePerfectGame() throws BowlingException {
		Game game= new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		Frame frame=new Frame(10,10);
		int i=game.getIndex();
		assertTrue((i==10) && (game.getFrameAt(i-1).isStrike()));
		game.setFirstBonusThrow(frame.getFirstThrow());
		game.setSecondBonusThrow(frame.getSecondThrow());
		assertEquals(300, game.calculateScore());
	}
	

}
