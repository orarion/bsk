package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test
	public void testImputFrame() throws BowlingException{
		int firstThrow=2;
		int secondThrow=4;
		Frame frame=new Frame(firstThrow,secondThrow);
		assertEquals("[2,4]",("["+frame.getFirstThrow()+","+frame.getSecondThrow()+"]"));
	}
	
	@Test
	public void testImputFrame2() throws BowlingException{
		int firstThrow=-1;
		int secondThrow=0;
		Frame frame=new Frame(firstThrow,secondThrow);
		assertEquals("[ , ]",frame.getFrame());
	}
	
	@Test
	public void testScoreFrame()throws BowlingException{
		int firstThrow=2;
		int secondThrow=6;
		Frame frame=new Frame(firstThrow,secondThrow);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testIsSpare() throws BowlingException {
		Frame frame=new Frame(1,9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testBonusSpare() throws BowlingException {
		Frame frame=new Frame(1,9);
		Frame frame1=new Frame(3,6);
		assertTrue(frame.isSpare());
		frame.setBonus(frame1.getFirstThrow());
		assertEquals(13,frame.getBonus());
		
	}
	
	@Test
	public void testScoreFrameSpare()throws BowlingException{
		Frame frame=new Frame(1,9);
		Frame frame1=new Frame(3,6);
		assertTrue(frame.isSpare());
		frame.setBonus(frame1.getFirstThrow());
		assertEquals(13,frame.getScore());
	}
	
	@Test
	public void testIsStrike() throws BowlingException {
		Frame frame=new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testBonusStrike() throws BowlingException {
		Frame frame=new Frame(10,0);
		Frame frame1=new Frame(3,6);
		assertTrue(frame.isStrike());
		frame.setBonus((frame1.getFirstThrow())+frame1.getSecondThrow());
		assertEquals(19,frame.getBonus());
		
	}
	
	@Test
	public void testScoreFrameStrike()throws BowlingException{
		Frame frame=new Frame(10,0);
		Frame frame1=new Frame(3,6);
		assertTrue(frame.isStrike());
		frame.setBonus((frame1.getFirstThrow())+frame1.getSecondThrow());
		assertEquals(19,frame.getScore());
	}

}
