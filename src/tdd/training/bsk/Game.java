package tdd.training.bsk;
import java.util.Arrays;

public class Game {
	private Frame[] gioco;
	private int indice;
	private Frame freme;
	
	/**
	 * It initializes an empty bowling game.
	 * @throws BowlingException 
	 */
	public Game() throws BowlingException  {
		Frame frame = new Frame(-1,0);
		gioco=new Frame[10];
		Arrays.fill(gioco,frame);
		indice=0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(indice>=10) {
			throw new BowlingException("non puoi aggiungere un altro frame"); 
		}else if (gioco[indice].getFirstThrow()==-1) {
			gioco[indice]=frame;
			indice++;
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return gioco[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		freme=new Frame(firstBonusThrow,0);
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		Frame f=freme;
		freme=new Frame (f.getFirstThrow(),secondBonusThrow);
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return freme.getFirstThrow();
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return freme.getFirstThrow()+freme.getSecondThrow();
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int s=0;
		for (int i=0;i<gioco.length;i++) {
			if(gioco[i].getScore()!=-1) {
				if(isSpare(i)) {
					ScoreSpare(i);
				}
				if(isStrike(i)) {
					ScoreStrike(i);
				}
				s+=gioco[i].getScore();
			}
		}
		return s;	
	}
	
	private boolean isSpare(int i) {
		return (gioco[i].isSpare());
	}

	private boolean isStrike(int i) {
		return (gioco[i].isStrike());
	}
	
	private boolean controlloGioco(int i) {
		return(gioco[i+1].getFirstThrow()!=-1);
	}
	
	private void ScoreStrike(int i) {
		if((i+1<gioco.length)) {
			if(controlloGioco(i)) {
				if(gioco[i+1].isStrike()&&(i+2)<gioco.length) {
					gioco[i].setBonus(gioco[i+1].getFirstThrow()+gioco[i+2].getFirstThrow());
				}else if(gioco[i+1].isStrike()&&i+2>=gioco.length) {
					gioco[i].setBonus(gioco[i+1].getFirstThrow()+getFirstBonusThrow());
				}else {
					gioco[i].setBonus(gioco[i+1].getFirstThrow()+gioco[i+1].getSecondThrow());
				}
			}else {
				gioco[i].setBonus(0);
			}
		}else if((i+1==gioco.length)) {
			gioco[i].setBonus(getSecondBonusThrow());
		}
	}
	
	private void ScoreSpare(int i) {
		if((i+1<gioco.length)) {
			if(controlloGioco(i)) {
				gioco[i].setBonus(gioco[i+1].getFirstThrow());
			}else {
				gioco[i].setBonus(0);
			}
		}else if((i+1==gioco.length)) {
			gioco[i].setBonus(getSecondBonusThrow());
		}
	}
	
	public String getgame() {
		String game=new String();
		game=gioco[0].getFrame();
		for(int i=1;i<gioco.length;i++) {
			game+=" "+gioco[i].getFrame();
		}
		return game;
		
	}
	public int getIndex() {
		return indice;
	}

}
