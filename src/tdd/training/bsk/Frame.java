package tdd.training.bsk;

public class Frame {

	private int primoTiro;
	private int secondoTiro;
	private int sbonus;
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if(((firstThrow<-1||firstThrow>10)||(secondThrow<0||secondThrow>10))&&((firstThrow+secondThrow)<=10)) {
			throw new BowlingException("frame non valido");
		}else {
			primoTiro=firstThrow;
			secondoTiro=secondThrow;
		}
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return primoTiro;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return secondoTiro;
	}
	
	public String getFrame() {
		if(getFirstThrow() ==-1) {
			return "[ , ]";
		}
		return "["+getFirstThrow()+","+getSecondThrow()+"]"; 
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		sbonus=10+bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return sbonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		if(isSpare()||isStrike()) {
			return getBonus();
		}else {
			return primoTiro+secondoTiro;
		}
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return ((primoTiro==10&&secondoTiro==0)&&(primoTiro+secondoTiro==10));
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return ((primoTiro>=0&&primoTiro<10)&&(secondoTiro<=(10-primoTiro))&&(primoTiro+secondoTiro==10));
	}

}
